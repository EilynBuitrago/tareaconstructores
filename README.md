# Manejo de Constructores
En Programación Orientada a Objetos (POO), un constructor es una subrutina cuya misión es inicializar un objeto de una clase. En el constructor se asignan los valores iniciales del nuevo objeto.

Se utiliza para crear tablas de clases virtuales y poder así desarrollar el polimorfismo, una de las herramientas de la programación orientada a objetos. Al utilizar un constructor, el compilador determina cual de los objetos va a responder al mensaje (virtual) que hemos creado. Tiene un tipo de acceso, un nombre y un paréntesis.

Sólo puede haber un método especial con el nombre de "constructor" en una clase. Un error de sintaxis será lanzado, si la clase contiene más de una ocurrencia de un método constructor. Un constructor puede utilizar la palabra clave super para llamar al constructor de una clase padre, si no especifica un método constructor, se utiliza un constructor predeterminado.

## Declaración de Un constructor
La declaración de un constructor diferente del constructor por defecto, obliga a que se le asigne el mismo identificador que la clase y que no se indique de forma explícita un tipo de valor de retorno. La existencia o no de parámetros es opcional. Por otro lado, la sobrecarga permite que puedan declararse varios constructores (con el mismo identificador que el de la clase), siempre y
146  A. García-Beltrán y J.M. Arranz cuando tengan un tipo y/o número de parámetros distinto. Por ejemplo, para la clase Fecha se
declaran dos constructores, el primero sin parámetros (por lo tanto se redefine el constructor por defecto).

## Que es una clase

Las clases son un pilar fundamental de la programación orientada a objetos. Permiten abstraer los datos y sus operaciones asociadas al modo de una caja negra. Los lenguajes de programación que soportan clases difieren sutilmente en su soporte para diversas características relacionadas con clases.

## Objetos

Se trata de un ente abstracto usado en programación que permite separar los diferentes componentes de un programa, simplificando así su elaboración, depuración y posteriores mejoras.

## Funciones

En programación, una función es una sección de un programa que calcula un valor de manera independiente al resto del programa. Una función tiene tres componentes importantes: los parámetros, que son los valores que recibe la función como entrada; el código de la función, que son las operaciones que hace la función.

## Se dejan algunos enlaces para aumentar el aprendizaje e enrriquecer los conocimientos.

-Para el manejo de constructores se deja el siguiente enlace en la pagina, este es un curso basico de you tube totalmente gratuito https://www.youtube.com/watch?v=s9ZFk4nrcxU.

- Se deja la siguiente pagina web con excelente información de estudio https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Classes/constructor.

#JavaScript
# Version 6 y 7
# Manejo de Constructores





