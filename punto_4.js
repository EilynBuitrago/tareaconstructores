class persona
{
    constructor(nombre,edad,eli,sexo,peso,altura)
    {
        this.nombre = nombre
        this.edad = edad
        this.eli = eli
        this.sexo = sexo
        this.peso = peso
        this.altura = altura
    }
    calcularIMC()
    {
        this.imc =  (this.peso/(this.altura * this.altura))
        if(this.imc < 20)
        {
            return -1
        }
        if(this.imc >= 20 && this.imc <= 25)
        {
            return 0
        }
        if(this.imc > 25)
        {
            return 1
        }

    }
    esMayorDeEdad()
    {
        if(this.edad >= 18)
        {
            return true
        }
        else
        {
            return false
        }
    }
    comprobarSexo()
    {
        if(this.sexo == "H" || this.sexo != "M")
        {
            this.sexo = "H"
            console.log("es Hombre")
        }
        else{
            console.log("es Mujer")
        }

    }
}

let person1 = new persona("Eilyn",17,"1002619297","H",20,2002)
console.log(person1.esMayorDeEdad())
person1.comprobarSexo()